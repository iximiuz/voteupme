function CommandParser()
{
    this._buf = '';
    this._header = null;
    this._command = null;
}

/**
 * {c: 'new_vote', bl: 100}{ ... vote ... }
 * @param chunk
 */
CommandParser.prototype.append = function(chunk) {
    console.log('CommandParser::chuck', chunk);

    this._buf += chunk.toString();
};

CommandParser.prototype.tryParse = function() {
    if (!this._headerParsed()) {
        if (!this._parseHeader()) {
            return false;
        }
    }

    return this._parseBody();
};

CommandParser.prototype.getCommand = function() {
    return this._command;
};

CommandParser.prototype._parseBody = function() {
    if (this._buf.length < this._header.bl) {
        return false;
    }

    var body = JSON.parse(this._buf);
    if (body) {
        this._command = {header: this._header, body: body};
        this._buf = '';
        this._header = null;
        return true;
    }

    return false;
};

CommandParser.prototype._parseHeader = function() {
    var openBracePos = this._buf.indexOf('{'),
        closeBracePos = this._buf.indexOf('}');

    if (-1 !== openBracePos && -1 !== closeBracePos) {
        this._header = JSON.parse(this._buf.substr(openBracePos, closeBracePos + 1));
        if (!this._header) {
            throw new Error('Bad header (JSON): ' + this._buf);
        }

        if (!this._header.c) {
            throw new Error('Bad header (Missed commandparser (c)): ' + this._buf);
        }

        if (!this._header.bl) {
            throw new Error('Bad header (Missed body lenght (bl)): ' + this._buf);
        }

        if (this._buf.length > closeBracePos + 1) {
            this._buf = this._buf.substr(closeBracePos + 1);
        }

        return true;
    }

    return false;
};

CommandParser.prototype._headerParsed = function() {
    return (null !== this._header);
};


module.exports = {
    ctr: CommandParser
};
