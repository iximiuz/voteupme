function Stat() {
    this.reqSpeedCounter = 0;
    this.reqTotalCount = 0;
    this.networkErrorsCount = 0;
    this.logicErrorsCount = 0;
}

Stat.prototype.start = function(sampleInterval) {
    this.sampleInterval = sampleInterval;

    var self = this;

    this.ticker = setInterval(
        function() {
            console.log(
                's:', (self.reqSpeedCounter / (self.sampleInterval / 1000)).toFixed(2),
                't:', self.reqTotalCount,
                'n/e:', self.networkErrorsCount,
                'l/e:', self.logicErrorsCount
            );

            self.reqTotalCount += self.reqSpeedCounter;
            self.reqSpeedCounter = 0;
        },
        sampleInterval
    );
};

Stat.prototype.incReq = function() {
    this.reqSpeedCounter++;
};

Stat.prototype.incNetErr = function() {
    this.networkErrorsCount++;
};

Stat.prototype.incLogicErr = function() {
    this.logicErrorsCount++;
};


module.exports = {
    ctr: Stat
};