function CommandDispatcher() {
    this.processors = {};
}

CommandDispatcher.prototype.registerProcessor = function(commandName, processor) {
    this.processors[commandName] = processor;
};

/**
 * @param command {header: {c: 'command_name', bl: <body_len>}, body: { ... }}
 */
CommandDispatcher.prototype.process = function(command) {
    console.log('CommandDispatcher::new_command', command);

    var processor = this.processors[command.header.c];
    if (!processor) {
        throw new Error('Unknown command: ' + JSON.stringify(command))
    }

    processor.process(command);

    console.log('CommandDispatcher::scheduled: ', JSON.stringify(command));
};


module.exports = {
    ctr: CommandDispatcher
};
