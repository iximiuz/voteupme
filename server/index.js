var HOST = '127.0.0.1';
var PORT = 8124;
var MONGO_DSN = 'mongodb://127.0.0.1:27017/vote_up_me';


var mongoClient = require(__dirname + '/../node_modules/mongodb/index.js').MongoClient,
    net = require('net'),
    Stat = require(__dirname + '/Stat.js').ctr,
    CommandParser = require(__dirname + '/CommandParser.js').ctr,
    CommandDispatcher = require(__dirname + '/CommandDispatcher.js').ctr,

    ProcessorEcho = require(__dirname + '/processors/ProcessorEcho.js').ctr,
    ProcessorGetVotes = require(__dirname + '/processors/ProcessorGetVotes.js').ctr,
    ProcessorGetVoterVotes = require(__dirname + '/processors/ProcessorGetVoterVotes.js').ctr,
    ProcessorVote = require(__dirname + '/processors/ProcessorVote.js').ctr,
    ProcessorGetVotesCount = require(__dirname + '/processors/ProcessorGetVotesCount.js').ctr,
    ProcessorRemoveVote = require(__dirname + '/processors/ProcessorRemoveVote.js').ctr;

var _s = new Stat();
_s.start(20000);

var server = net.createServer(function(clientSocket) {
    _s.incReq();

    console.log('Server::new_connection');

    var end = clientSocket.end;
    clientSocket.end = function() {
        console.log('Processor::processed', arguments);

        end.apply(clientSocket, arguments);
    };

    var dispatcher = new CommandDispatcher(),
        parser = new CommandParser(), command;

    //TODO: вынести опредление имени коллекции и MONGO_DSN в зависимости от параметра client.
    // сделать Virtual Shards по клиентам
    dispatcher.registerProcessor('echo', new ProcessorEcho(clientSocket));
    dispatcher.registerProcessor('vote', new ProcessorVote(clientSocket, mongoClient, MONGO_DSN, 'objects_votes'));
    dispatcher.registerProcessor('get_votes', new ProcessorGetVotes(clientSocket, mongoClient, MONGO_DSN, 'objects_votes'));
    dispatcher.registerProcessor('get_voter_votes', new ProcessorGetVoterVotes(clientSocket, mongoClient, MONGO_DSN, 'objects_votes'));
    dispatcher.registerProcessor('get_votes_count', new ProcessorGetVotesCount(clientSocket, mongoClient, MONGO_DSN, 'objects_votes'));
    dispatcher.registerProcessor('remove_vote', new ProcessorRemoveVote(clientSocket, mongoClient, MONGO_DSN, 'objects_votes'));

    clientSocket.on('data', function(buffer) {
        try {
            parser.append(buffer.toString());
            if (parser.tryParse()) {
                command = parser.getCommand();
                dispatcher.process(command);
            }
        } catch (e) {
            console.log('Server::catch_error', e, e.stack);
            _s.incLogicErr();

            clientSocket.end(JSON.stringify({error: true, msg: JSON.stringify(e)}));
        }
    });

    clientSocket.on('error', function(error) {
        console.log('Server::net_error', error);
        _s.incNetErr();

        clientSocket.end();
    });

    clientSocket.on('end', function() {
        console.log('Server::client_disconnected');
    });
});

server.listen(PORT, HOST, function() {
    console.log('Server::listen_on: ' + HOST + ':' + PORT);
});