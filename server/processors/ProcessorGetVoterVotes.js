function ProcessorGetVoterVotes(clientSocket, mongo, dsn, collectionName) {
    this.mongo = mongo;
    this.clientSocket = clientSocket;
    this.dsn = dsn;
    this.collectionName = collectionName;
}

ProcessorGetVoterVotes.prototype.process = function(command) {
    this.checkCommand(command);

    var self = this;

    this.mongo.connect(this.dsn, function(err, db) {
        if (err) {
            throw err;
        }

        var collection = db.collection(self.collectionName),
            commandBody = command.body,
            tagsSpecified = (commandBody.tags && Array.isArray(commandBody.tags) && commandBody.tags.length),
            tags = tagsSpecified ? commandBody.tags : ['dflt'],
            offset = commandBody.offset || 0,
            limit = commandBody.limit || 100,
            query = {};

        if (commandBody.voteeType) {
            query._id = {$regex: commandBody.voteeType + ':.+'};
        }

        // todo: переписать на использование $or, чтобы была возможность работы с несколькими тегами в одном запросе.
        for (var k in tags) if (tags.hasOwnProperty(k)) {
            query['tags.' + tags[k] + '.vtrs.' + commandBody.voterId] = {$exists: true};
        }

        collection.find(query).skip(offset).limit(limit).toArray(
            function(err, records) {
                if (err) {
                    db.close();
                    throw err;
                }

                var result = [];
                for (var i = 0, l = records.length; i < l; i++) {
                    var vote = {tags: {}};
                    vote.voteeType = records[i]._id.split(':');

                    vote.voteeId = vote.voteeType[1];
                    vote.voteeType = vote.voteeType[0];

                    for (var k in tags) if (tags.hasOwnProperty(k)) {
                        if (records[i].tags[tags[k]] && records[i].tags[tags[k]].vts) {
                            var tagVotes = [];
                            records[i].tags[tags[k]].vts.forEach(function(v) {
                                if (v.voterId == commandBody.voterId) {
                                    tagVotes.push(v);
                                }
                            });
                        }

                        if (tagVotes && tagVotes.length) {
                            vote.tags[tags[k]] = tagVotes;
                            tagVotes = null;
                        }
                    }

                    result.push(vote);
                }

                self.clientSocket.end(JSON.stringify(result));
                db.close();
            }
        );
    });
};

ProcessorGetVoterVotes.prototype.checkCommand = function(command) {
    if (undefined === command.body.voteeType || undefined === command.body.voterId) {
        throw new Error('Wrong command: ' + JSON.stringify(command));
    }
};

module.exports = {
    ctr: ProcessorGetVoterVotes
};