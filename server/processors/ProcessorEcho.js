function ProcessorEcho(clientSocket) {
    this.clientSocket = clientSocket;
}

ProcessorEcho.prototype.process = function(command) {
    this.clientSocket.end(JSON.stringify(command));
};


module.exports = {
    ctr: ProcessorEcho
};
