function ProcessorVote(clientSocket, mongo, dsn, collectionName) {
    this.mongo = mongo;
    this.clientSocket = clientSocket;
    this.dsn = dsn;
    this.collectionName = collectionName;
}

ProcessorVote.prototype.process = function(command) {
    this.checkCommand(command);

    var self = this;

    this.mongo.connect(this.dsn, function(err, db) {
        if (err) {
            throw err;
        }

        var collection = db.collection(self.collectionName),
            commandBody = command.body,
            voteeEntryId = commandBody.voteeType + ':' + commandBody.voteeId,
            tag = 'tags.'+ (commandBody.tag || 'dflt'),
            incClause = {},
            pushClause = {},
            setClause = {};

        incClause[tag + '.cntrs.' + commandBody.value] = 1;

        pushClause[tag + '.vts'] = {
            voterId: commandBody.voterId,
            value: commandBody.value,
            createdAt: commandBody.createdAt
        };

        setClause[tag + '.vtrs.' + commandBody.voterId] = commandBody.value.toString();

        var update = {
            $inc: incClause,
            $push: pushClause,
            $set: setClause
        };

        collection.update(
            {_id: voteeEntryId},
            update,
            {upsert: true, getLastError: 1, safe: true},
            function(err) {
                if (err) {
                    db.close();

                    if (11000 === err.code) {
                        self.clientSocket.end(JSON.stringify({error: true, msg: 'Already voted', code: 10001}));
                    } else {
                        throw JSON.stringify({err: err, query: {_id: voteeEntryId}, update: update, command: command});
                    }
                }
                
                self.clientSocket.end(JSON.stringify({result: true}));
            }
        );
    });
};

ProcessorVote.prototype.checkCommand = function(command) {
    if (
           undefined === command.body.voterId
        || undefined === command.body.voteeType
        || undefined === command.body.voteeId
        || undefined === command.body.createdAt
        || undefined === command.body.value
        ) {
        throw new Error('Wrong command: ' + JSON.stringify(command));
    }
};

module.exports = {
    ctr: ProcessorVote
};
