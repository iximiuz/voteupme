function ProcessorRemoveVote(clientSocket, mongo, dsn, collectionName) {
    this.mongo = mongo;
    this.clientSocket = clientSocket;
    this.dsn = dsn;
    this.collectionName = collectionName;
}

ProcessorRemoveVote.prototype.process = function(command) {
    this.checkCommand(command);

    var self = this;

    this.mongo.connect(this.dsn, function(err, db) {
        if (err) {
            throw err;
        }

        var collection = db.collection(self.collectionName),
            commandBody = command.body,
            voteeEntryId = commandBody.voteeType + ':' + commandBody.voteeId,
            tag = 'tags.'+ (commandBody.tag || 'dflt'),
            incClause = {},
            pullClause = {},
            unsetClause = {},
            whereClause = {};

        incClause[tag + '.cntrs.' + commandBody.value] = -1;

        pullClause[tag + '.vts'] = {
            voterId: commandBody.voterId,
            value: commandBody.value
        };

        unsetClause[tag + '.vtrs.' + commandBody.voterId] = true;

        var update = {
            $inc: incClause,
            $pull: pullClause,
            $unset: unsetClause
        };

        whereClause._id = voteeEntryId;
        whereClause[tag + '.cntrs.' + commandBody.value] = {'$gt': 0};

        collection.update(
            whereClause,
            update,
            {getLastError: 1, safe: true},
            function(err, result) {
                if (err) {
                    db.close();

                    throw JSON.stringify({err: err, query: whereClause, update: update, command: command});
                }

                self.clientSocket.end(
                    JSON.stringify((1 == result)
                        ? {result: true}
                        : {error: true, msg: 'Vote not found', code: 10002}
                    )
                );
            }
        );
    });
};

ProcessorRemoveVote.prototype.checkCommand = function(command) {
    if (
           undefined === command.body.voterId
        || undefined === command.body.voteeType
        || undefined === command.body.voteeId
        || undefined === command.body.value
        ) {
        throw new Error('Wrong command: ' + JSON.stringify(command));
    }
};

module.exports = {
    ctr: ProcessorRemoveVote
};
