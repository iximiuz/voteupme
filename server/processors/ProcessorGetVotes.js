function ProcessorGetVotes(clientSocket, mongo, dsn, collectionName) {
    this.mongo = mongo;
    this.clientSocket = clientSocket;
    this.dsn = dsn;
    this.collectionName = collectionName;
}

/**
 * {
 *     _id: "activity:101_53705",
 *     tags: {
 *         dflt: {
 *              cntrs: {"-1":100, "1": 9999},
 *              vts: [{voterId:104, createdAt: "...", value: -1}, ...],
 *              vtrs: {
 *                  104: true,
 *                  102: false,
 *                  103: true,
 *                  ...
 *              }
 *          },
 *          tag1: { ... },
 *          ...
 *     }
 * }
 *
 */

/**
 * @param command {
 *                    voteeType: "activity",
 *                    voteeId: 100_500,
 *                    [[tags: Array(tag1, ...)], [limit: 100], [offset: 0]] | [[tags: Array({tag: "tag1", [limit: 100], [offset: 0]})]]
 *                }
 */
ProcessorGetVotes.prototype.process = function(command) {
    this.checkCommand(command);

    var self = this;

    this.mongo.connect(this.dsn, function(err, db) {
        if (err) {
            throw err;
        }

        var collection = db.collection(self.collectionName),
            commandBody = command.body,
            voteeEntryId = commandBody.voteeType + ':' + commandBody.voteeId,
            tagsSpecified = (commandBody.tags && Array.isArray(commandBody.tags) && commandBody.tags.length),
            tags = tagsSpecified ? commandBody.tags : ['dflt'],
            tag,
            tagsPlain = [],
            fields = {};


        for (var k in tags) {
            if (!tags.hasOwnProperty(k)) {
                continue;
            }

            tag = tags[k];
            tagsPlain.push(tag);

            if ('string' === typeof tag) {
                fields['tags.' + tag + '.vts'] = {$slice: [commandBody.offset || 0, commandBody.limit || 100]};
            } else {
                fields['tags.' + tag.tag + '.vts'] = {$slice: [tag.offset || 0, tag.limit || 100]};
            }
        }

        collection.find({_id: voteeEntryId}, fields).toArray(
            function(err, record) {
                if (err) {
                    db.close();
                    throw err;
                }

                var result = {
                    voteeType: commandBody.voteeType,
                    voteeId: commandBody.voteeId
                };

                record = record.length ? record[0] : [];
                if (tagsSpecified) {
                    result.votes = {};
                    for (var t in tagsPlain) {
                        if (!tagsPlain.hasOwnProperty(t)) {
                            continue;
                        }

                        tag = tagsPlain[t];
                        result.votes[tag] = (record.tags && record.tags[tag])
                            ? record.tags[tag].vts
                            : [];
                    }
                } else {
                    result.votes = (record.tags && record.tags.dflt)
                        ? record.tags.dflt.vts :
                        [];
                }

                self.clientSocket.end(JSON.stringify(result));
                db.close();
            }
        );
    });
};

ProcessorGetVotes.prototype.checkCommand = function(command) {
    if (undefined === command.body.voteeType || undefined === command.body.voteeId) {
        throw new Error('Wrong command: ' + JSON.stringify(command));
    }
};

module.exports = {
    ctr: ProcessorGetVotes
};