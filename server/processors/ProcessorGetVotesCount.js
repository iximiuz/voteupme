function ProcessorGetVotesCount(clientSocket, mongo, dsn, collectionName) {
    this.mongo = mongo;
    this.clientSocket = clientSocket;
    this.dsn = dsn;
    this.collectionName = collectionName;
}

/**
 * {
 *     _id: "activity:101_53705",
 *     tags: {
 *         dflt: {
 *              cntrs: {"-1":100, "1": 9999},
 *              vts: [{voterId:104, createdAt: "...", value: -1}, ...],
 *              vtrs: {
 *                  104: true,
 *                  102: false,
 *                  103: true,
 *                  ...
 *              }
 *          },
 *          tag1: { ... },
 *          ...
 *     }
 * }
 *
 */

/**
 * @param command {
 *                    voterId: 101,
 *                    voteeType: "activity",
 *                    voteeIds: [101_500, 99_876, ...],
 *                    (tags: [tag1, tag2, ...])
 *                }
 *
 *                {
 *                    voterId: 101,
 *                    votees: [{type: "activity", id: 101_500, tags: ([tag1, tag2, ...])}, ...]
 *                }
 */
ProcessorGetVotesCount.prototype.process = function(command) {
    this.checkCommand(command);

    var self = this;

    this.mongo.connect(this.dsn, function(err, db) {
        if (err) {
            throw err;
        }

        var collection = db.collection(self.collectionName),
            cmdBody = command.body,
            voterSpecified = (undefined !== cmdBody.voterId),
            voterId = cmdBody.voterId,
            voteeIds = [],
            tags;

        if (cmdBody.voteeType) {
            voteeIds = cmdBody.voteeIds.map(function(id) {
                return cmdBody.voteeType + ':' + id;
            });

            tags = (cmdBody.tags && Array.isArray(cmdBody.tags) && cmdBody.tags.length)
                ? cmdBody.tags
                : ['dflt'];
        } else {
            tags = {};

            cmdBody.votees.forEach(function(votee) {
                voteeIds.push(votee.type + ':' + votee.id);
                if (votee.tags && Array.isArray(votee.tags) && votee.tags.length) {
                    votee.tags.forEach(function(tag) {
                        tags[tag] = tag;
                    })
                }
            });

            tags = Object.keys(tags);
            tags.push('dflt');
        }

        var projectClause = {_id: 1};

        tags.forEach(function(tag) {
            projectClause['tags.' + tag + '.cntrs'] = 1;
            if (voterSpecified) {
                projectClause['tags.' + tag + '.vtrs.' + voterId] = 1;
            }
        });


        collection.aggregate(
            [
                {$match: {_id: {$in: voteeIds}}},
                {$project: projectClause}
            ],
            {},
            function(err, records) {
                if (err) {
                    db.close();
                    throw err;
                }

                //var util = require('util');
                //console.log(util.inspect(records, false, null));

                var response, id;
                if (cmdBody.voteeType) {
                    response = {voteeType: cmdBody.voteeType};

                    if (cmdBody.tags) {
                        response.tags = {};

                        records.forEach(function(r) {
                            var tags = Object.keys(r.tags);
                            id = r._id.split(':')[1];

                            tags.forEach(function(tag) {
                                if (r.tags[tag]) {
                                    response.tags[tag] = response.tags[tag] || {counters: {}, voted: {}};
                                    response.tags[tag].counters[id] = r.tags[tag].cntrs;
                                    if (voterSpecified && r.tags[tag].vtrs && undefined !== r.tags[tag].vtrs[voterId]) {
                                        response.tags[tag].voted[id] = r.tags[tag].vtrs[voterId];
                                    }
                                }
                            });
                        });
                    } else {
                        response.counters = {};
                        response.voted = {};

                        records.forEach(function(r) {
                            if (r.tags.dflt) {
                                id = r._id.split(':')[1];

                                response.counters[id] = r.tags.dflt.cntrs;
                                if (voterSpecified && r.tags.dflt.vtrs && undefined !== r.tags.dflt.vtrs[voterId]) {
                                    response.voted[id] = r.tags.dflt.vtrs[voterId];
                                }
                            }
                        });
                    }
                } else {
                    response = [];

                    var recordsById = {}, record;
                    records.forEach(function(r) {
                        recordsById[r._id] = r;
                    });

                    cmdBody.votees.forEach(function(votee) {
                        var entry = {type: votee.type, id: votee.id};

                        record = recordsById[votee.type + ':' + votee.id];
                        if (record) {
                            if (votee.tags) {
                                entry.tags = {};
                                Object.keys(record.tags).forEach(function(tag) {
                                    if (-1 !== votee.tags.indexOf(tag)) {
                                        entry.tags[tag] = {counters: record.tags[tag].cntrs};
                                        if (voterSpecified && record.tags[tag].vtrs && undefined !== record.tags[tag].vtrs[voterId]) {
                                            entry.tags[tag].voted = record.tags[tag].vtrs[voterId];
                                        }
                                    }
                                });
                            } else if (record.tags.dflt) {
                                entry.counters = record.tags.dflt.cntrs;
                                if (voterSpecified && record.tags.dflt.vtrs && undefined !== record.tags.dflt.vtrs[voterId]) {
                                    entry.voted = record.tags.dflt.vtrs[voterId];
                                }
                            }
                        }

                        response.push(entry);
                    });
                }

                //console.log(util.inspect(response, false, null));
                self.clientSocket.end(JSON.stringify(response));
            }
        );
    });
};

ProcessorGetVotesCount.prototype.checkCommand = function(command) {
    if (
        undefined === command.body.votees
        && (undefined === command.body.voteeType && undefined === command.body.voteeIds)
    ) {
        throw new Error('Wrong command: ' + JSON.stringify(command));
    }
};

module.exports = {
    ctr: ProcessorGetVotesCount
};